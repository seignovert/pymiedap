{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to EXOPy\n",
    "\n",
    "## Installation\n",
    "\n",
    "The latest version of EXOPy is incorporated within the PyMieDAP program. Detailed instructions on how to install PyMieDAP are avaiable through its [documentation file](../pymiedap_tutorial.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview of the model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A simplified flow diagram sketching the various components of EXOPy and their order of use, the overall inputs/outputs, as well as the interaction with the PyMieDAP tool, is found below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![alt text](exopy_diagram.png \"Big Picture\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All computations carried out by the EXOPy tool make use of what we call the 'body' type of python objects. These serve as container for all input parameters and output values, and as interface with the EXOPy functions, for each body involved in the planetary system.\n",
    "\n",
    "Besides the user needs to specify a set of basic model settings such as which is the observer position with respect to the planetary system reference frame or which is the reference body used for combining the reflected starlight. More details on this operatinos are given afterwards in this tutorial.\n",
    "\n",
    "Once defined and specified the inputs for each body object, the computation of the orbits can be conducted, as well as the determination of the geometric values which are required for further steps. Afterwards, the pixel shadowing of each body due to phase angle, transit and/or eclipse events are computed as follows from the flowchart.\n",
    "\n",
    "Finally, the flux and polarization status of the reflected starlight by each body is computed by using some of the modules at PyMieDAP and the various results can be merged in a single planetary system signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting started"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The EXOPy model is contained in the PyMieDAP model folder. To import it, the user can type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import pymiedap.exopy as exopy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "being exopy the acronym employed for the model.\n",
    "\n",
    "The functions contained in the exopy tool are:\n",
    "\n",
    " * **exopy.new_body:** Function generating 'body objects'.\n",
    " * **exopy.new_system:** Function loading a pre-defined system of 'body' objects.\n",
    " * **exopy.cfg:** Configuration parameters of the EXOPy model.\n",
    " * **exopy.compute:** EXOPy object containing all functions required to perform the simulation.\n",
    " * **exopy.plot:** EXOPy object containing shorcut functions for visualization of results.\n",
    " * **exopy.run_simulation:** Pre-defined script containing basic instructions for running an EXOPy simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step towards carrying out the required simulations is the definition of the bodies involved. In this particular case, we're trying to model the Earth-Moon-Sun system, so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth = exopy.new_body('Earth', 'planet')\n",
    "Moon  = exopy.new_body('Moon' , 'moon'  )\n",
    "Sun   = exopy.new_body('Sun'  , 'star'  )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Basic properties of these bodies which need to be provided to EXOPy are found under the *properties* category, i.e. radius and mass (*R*, *m*)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth.properties.R = 6370E3  # [m]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Earth.properties.m = 6E24    # [kg]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "This input values, together with the orbital elements of the bodies, can be stored at the exopy.py script and therefore retrieved through the *exopy.new_system* functions as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "[Moon, Earth, Sun], conf = exopy.new_system('s_system')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where 's_system' is the string associated to the solar system predefined scenario (Earth-Moon-Sun system)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Moon.orbital_elements.omega = 180\n",
    "Earth.orbital_elements.omega_b = 300"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before continuing, the user needs to set up a couple of model configuration parameters:\n",
    "- **Observer's location:** characterized by the spherical coordinates given by an azimuth and elevation angle in degrees."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "conf.az = 0 # [deg]\n",
    "conf.el = 0 # [deg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- **Reference elements:** reference line and reference body for the computation of reflected starlight."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "conf.ref_body = 'Planet'\n",
    "conf.ref_line = 'Planet'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Documentation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All functions, methods and objects in EXOPy are accompanied with a brief documentation providing information regarding the purpose, useage and/or parameteres stored in each case, as well as units and format information. \n",
    "\n",
    "The user can access this documentation by typing '?', e.g.:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "exopy?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "exopy.new_system?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Earth.grid?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, methods and objects can be mentioned and/or called for getting generic information regarding the parameteres and variables involved or specific values at a particular time (pixel) index, e.g.:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "Earth.properties"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Being the orbital elements already pre-defined, the user can print an overview of them as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth.orbital_elements"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Moon.orbital_elements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These values can be modified at any moment. For this, it is required to understand the conventions established, explained below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining and computing the orbit position"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The orbital motion of the moon and planet around the star is computed under the assumption of a Nested Two Body Proble, i.e. it is assumed that the star does not alter the movement of the moon around the planet-moon system barycentre, while the barycentre of the system describes a Kepler orbit around the star.\n",
    "\n",
    "The input orbital elements required for carrying out the orbits computation is different for planet and moon type of bodies, while the star does not play any role. By convention, for a planet type of object we store the information of the planet-moon system barycentre:\n",
    "\n",
    " - **a_b:** semi-major axis of the planet-moon system barycentre's orbit around the star [m] (float)\n",
    " - **e_b:** eccentricity of the planet-moon system barycentre's orbit around the star [-] (float)\n",
    " - **i_b:** inclination of the planet-moon system barycentre's orbit around the star [deg] (float)\n",
    " - **Omega_b:** Right Ascension of the Ascending Node of the planet-moon system barycentre's orbit around the star [deg] (float)\n",
    " - **omega_b:** Argument of periapsis of the planet-moon system barycentre's orbit around the star [deg] (float)\n",
    " - **t0_b:** time since last periapsis passage of the planet-moon system barycentre's orbit around the star [s] (float)\n",
    " \n",
    "In the case of the moon, the information of the moon's orbit around the planet-moon system barycentre is stored as:\n",
    "\n",
    " - **a:** semi-major axis of the moon's orbit around the planet-moon system barycentre [m] (float)\n",
    " - **e:** eccentricity of the moon's orbit around the planet-moon system barycentre [-] (float)\n",
    " - **i:** inclination of the moon's orbit around the planet-moon system barycentre [deg] (float)\n",
    " - **Omega:** Right Ascension of the Ascending Node of the moon's orbit around the planet-moon system barycentre [deg] (float)\n",
    " - **omega:** Argument of periapsis of the moon's orbit around the planet-moon system barycentre [deg] (float)\n",
    " - **t0:** time since last periapsis passage of the moon's orbit around the planet-moon system barycentre [s] (float)\n",
    " \n",
    "Specified these values, the user can easily compute the position of the bodies via the compute.orbit exopy function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "[Moon, Planet, Star] = exopy.compute.orbit(Moon, Earth, Sun, delta_t = 60*60*24*10, final_t = 365*60*60*24)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where the last two input parameters are the time step and final time of computation in seconds.\n",
    "\n",
    "The employed function computes the 3D and 2D position of each body with time and stores it inside the *ephemeris* category. However, it does not compute the angles of interest. For this, use is made of the *exopy.compute.geometry* function. \n",
    "\n",
    "Use can be made of the exopy.plot functions to get an overview of the computed orbits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#exopy.plot.XYZorbit(Earth.ephemeris.position3D_s, Moon.ephemeris.position3D_s)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "exopy.plot.xyorbit(Moon.ephemeris.position2D_mp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# from pymiedap.exopy_functions import display_animation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# animation = display_animation(exopy.plot.anim_orbit(Moon.ephemeris.time, Moon.ephemeris.position3D_s));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# animation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Defining and computing the geometry of the problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "As a first step towards the computation of the full geometry of the problem (pixel and time dependent) is the definition of the grids to be used for the planet and moon bodies through the function *set_grid*. In this case, a regular squared pixels grid of 30 elements along the equator is set for the planet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth.grid.set_grid(grid_type = 'square', Nsq = 30)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth.grid.show_grid()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "For the moon, the closest integer number of pixels along the equator that matches the ratio of radii between the planet annd moon is selected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "Moon.grid.set_grid(grid_type = 'square', Nsq = int(np.round(Earth.grid.Nsq*Moon.properties.R/Earth.properties.R)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Moon.grid.show_grid()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth, Moon, Sun= exopy.compute.geometry([Earth, Moon, Sun], conf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The new information generated is stored under the category *geometry* of each body. For instance, at time index 100 we have for planet Earth:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth.geometry(100, unit = 'deg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, the planet and moon bodies are considered to be fully illuminated as seen from the observer's position, as the plot below shows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "exopy.plot.shadow_d(Earth, conf, t=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to computed the pixel darkening due to shadowing effects the user can employ the phase, transits and eclipses funtions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computing the pixel darkenning "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "EXOPy allows to compute the pixel darkenning due to three different sources:\n",
    "\n",
    "- **Phase angle:** Along the orbit, just part of the body will be visible from the observer's position. The function *exopy.compute.phases* takes care of this factor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth, Moon = exopy.compute.phases([Earth, Moon], Sun, conf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- **Transit events:** The interposition of one body between the targeted body and the observer results in a partial (or total) blockage of the reflected light:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Moon, Earth, Sun = exopy.compute.transits([Moon, Earth, Sun], conf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- **Eclipse events:** The interposition of one body between the targeted body and the star results in a gradual blockage of the reflected light:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Moon, Earth = exopy.compute.eclipses([Moon, Earth], Sun, conf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In our example, we can observe the moon eclipsing the Earth at time index equal 0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "exopy.plot.shadow_d(Earth, conf, t = 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, we can compute the reflected starlight."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting the atmosphere and surface properties"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Computing the reflected starlight of each body requires to specify which are the surface and atmosphere properties as well as at which wavelength are we working on. For this, use is made of the PyMieDAP *atmosphere* class which can be configured according to the advice given in [here](pymiedap_tuto.html). \n",
    "\n",
    "Alternatively, the user can employ one of the pre-defined atomospheric models and tune it through any of the available input values using the exopy module called *atm_model*.\n",
    "\n",
    "In this case, the Earth has been granted with a cloudy model in the blue, while a lambertian surface model of albedo 0.1 has been employed for the Moon surface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth.atmosphere = exopy.atm_models.Earth()[0]\n",
    "Moon.atmosphere  = exopy.atm_models.Moon(alb = 0.1)[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computing reflected starlight"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a last step, the reflected starlight at each pixel is integrated along the entire disk for computing the polarized reflected signal of each body independently. This is done through the function *exopy.compute.int_radiance* as:\n",
    "\n",
    "**! Mind that this process can take a long time depending on the Fourier files to be read, the number of pixels, and the time vector to be covered.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "[Earth, Moon] = exopy.compute.int_radiance([Earth, Moon])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As output, the discretized reflected signals as well as the integrated signals are obtained. These can be easily plotted through some of the pre-defined display functions as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "exopy.plot.radiance_d(Earth, conf, cmap=plt.cm.viridis)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "exopy.plot.radiance(Earth)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the individual signals are combined taking one particular body and one particular scattering plane as reference through the *exopy.compute.combine* function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "I,Q,U,V,P,Chi = exopy.compute.combine([Earth, Moon], conf)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Earth.geometry.ref_plane_to_ref_line_angle"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "Moon.geometry.ref_plane_to_ref_line_angle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, the final results can be displayed for the overall planetary system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "exopy.plot.detail_radiance([Earth,Moon], I, Q, U, V)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "exopy.plot.radiance(Earth,phase=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "exopy.plot.radiance(Moon,phase=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
